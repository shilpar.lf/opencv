import json

# some JSON:
x =  '{ "name":"kalpana",' \
     ' "age":30, ' \
     '"city":"New York",' \
     '"married":"True",' \
     '"children":2,' \
     '"cars":[{"model":"bmw","price":"90k"}]}'

# parse x:
# y = json.loads(x)
# y=json.dumps(x)
# the result is a Python dictionary:
# print(y)
# print(json.dumps(x, indent=4,separators=(".","=")))
print(json.dumps(x, indent=4, separators=(". ", " = ")))
