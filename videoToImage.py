import cv2
import pytesseract
from pytesseract import Output
# Open video file
video = cv2.VideoCapture(r"C:\Users\admin\PycharmProjects\pythonProject\images\car_video4.mp4")

# Define the output image path
output_path = r" C:\Users\admin\PycharmProjects\pythonProject\data\picture"


pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
cascade = cv2.CascadeClassifier('haarcascades_haarcascade_russian_plate_number.xml')



# Initialize a counter for image file names
image_counter = 0

while True:
    # Read a frame from the video
    ret, frame = video.read()

    if not ret:
        break

    # Save the frame as an image
    image_path = output_path + "image_" + str(image_counter) + ".jpg"
    cv2.imwrite(image_path, frame)

    # Display the frame in a window
    cv2.imshow("Video to Image Conversion", frame)

    # Increment the image counter
    image_counter += 1
    plates = cascade.detectMultiScale(frame, 3.3, 7)  # line is used to crop the number plate to identify numbers
    # gray_plates = frame[y:y + h, x:x + w]
    #
    # text = pytesseract.image_to_string(gray_plates)
    #
    # print("plate number: ", text[:-1])
    for (x, y, w, h) in plates:
        # draw bounding rectangle around the license number plate
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        gray_plates = frame[y:y + h, x:x + w]
        color_plates = frame[y:y + h, x:x + w]

        # save number plate detected
        cv2.imwrite('Numberplate1.jpg', gray_plates)
        cv2.imshow('Number Plate1', gray_plates)
        cv2.imshow('Number Plate Image1', gray)
        text = pytesseract.image_to_string(gray_plates)

        print("plate number: ", text[:-1])

    # Wait for a key press and exit if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the video capture and close the window
video.release()
cv2.destroyAllWindows()

# def employee_details(self):
#     for employee in self.employees:
#         print(employee)

# def search_employee_by_gender(self, gender):
#     matching_employees = [employee for employee in self.employees if employee.gender.lower() == gender.lower()]
#     if matching_employees:
#         print("Employees with gender", gender + ":")
#         for employee in matching_employees:
#             print(employee.name)
#     else:
#         print("No employees found with the given gender.")