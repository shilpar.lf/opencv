import cv2
import numpy as np

# Load YOLO
net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# Load video
video_path = "video.mp4"
cap = cv2.VideoCapture(video_path)

# Define output video codec and create VideoWriter object
output_path = "output_video.avi"
fourcc = cv2.VideoWriter_fourcc(*'XVID')
fps = cap.get(cv2.CAP_PROP_FPS)
frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
out = cv2.VideoWriter(output_path, fourcc, fps, (frame_width, frame_height))

# Load number plate detection model
number_plate_model = cv2.CascadeClassifier('haarcascade_russian_plate_number.xml')

# Process video frames
while True:
    ret, frame = cap.read()

    if not ret:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Perform number plate detection
    number_plates = number_plate_model.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(50, 50))

    # Process each detected number plate
    for (x, y, w, h) in number_plates:
        # Draw bounding box around the number plate
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Extract the number plate region
        number_plate_roi = gray[y:y + h, x:x + w]

        # Apply additional processing on the number plate region if needed

        # Display the extracted number plate region
        cv2.imshow("Number Plate", number_plate_roi)

    # Write the frame with number plates to the output video
    out.write(frame)

    # Display the resulting frame
    cv2.imshow("Number Plate Detection", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the resources
cap.release()
out.release()
cv2.destroyAllWindows()
