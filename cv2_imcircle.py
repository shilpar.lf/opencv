import cv2


path=r'natural_image.jpg'
image=cv2.imread(path)
window_name='Image'

center_coordinates=(200,150)
radious=100
color=(0,250,0)
thickness=-1
image=cv2.circle(image,center_coordinates,radious,color,thickness)
cv2.imshow(window_name,image)
cv2.waitKey(0)
cv2.destroyAllWindows()
