# import pytesseract as tess
#
# from PIL import Image
# tess.pytesseract.tesseract_cmd = r'tesseract-ocr-w64-setup.exe'
#
# img = Image.open('text.png')
# text = tess.image_to_string(Image.open(img))
#
# print(text)

# Import libraries
from PIL import Image
import pytesseract
import cv2
import numpy as np
from pytesseract import Output
import os

#Specifies PATH of Tesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Simply extracting text from image
image = Image.open("text.png")
# image = image.resize((300,150))
# custom_config = r'-l eng --oem 3 --psm 6'
text = pytesseract.image_to_string(image)
print(text)
# cv2.imshow('Text',image )
# cv2.waitKey(0)
# cv2.destroyAllWindows()