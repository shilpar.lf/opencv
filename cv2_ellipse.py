# Python program to explain cv2.ellipse() method

# importing cv2
import cv2

path = r'river.jpg'

image = cv2.imread(path)

window_name = 'Image'

center_coordinates = (700, 400)#inside left and upper

axesLength = (100, 50) #ellipse length and width

angle = 0 #Rotation of the ellipse

startAngle = 0  #starting ellipse gap

endAngle = 360 #end of the angle
color = (0, 0, 255)
thickness = 5
image = cv2.ellipse(image, center_coordinates, axesLength,
                    angle, startAngle, endAngle, color, thickness)
cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()