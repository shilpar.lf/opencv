# Python program to explain cv2.arrowedLine() method

# importing cv2
import cv2

path = r'natural_image.jpg'

image = cv2.imread(path)

window_name = 'Image'

start_point = (0, 0)

end_point = (500, 400)
color = (0, 255, 0)
thickness = 9
# tipLength=0.5 =>arrow edge point length
image = cv2.arrowedLine(image, start_point, end_point,color, thickness,tipLength = 0.5)
cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()