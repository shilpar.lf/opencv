# import sklearn
# import pandas as pd
# from sklearn.datasets import load_iris
# iriss = load_iris()
# df_iris = pd.DataFrame(iriss.data, columns=iriss.feature_names)
# df_iris.head()
# df_iris.tail()
# df_iris.dtypes

# from pandas.plotting import scatter_matrix
# import matplotlib.pyplot as plt
# scatter_matrix(df_iris,figsize=(10,10))
# plt.show()
#
# # Import scikit learn
# from sklearn import datasets
# # Load data
# iris= datasets.load_iris()
# # Print shape of data to confirm data is loaded
# print(iris.data.shape)

from sklearn import svm
from sklearn import datasets

# # Load dataset
# iris = datasets.load_iris()
# clf = svm.LinearSVC()
# # learn from the data
# clf.fit(iris.data, iris.target)
# # predict for unseen data
# clf.predict([[5.0, 3.6, 1.3, 0.25]])
# # Parameters of model can be changed by using the attributes ending with an underscore
# print(clf.coef_)

import pandas as pd
from sklearn.datasets import load_iris
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
iris = load_iris()
X = iris.data
y = iris.target
feature_names = iris.feature_names
target_names = iris.target_names
print("Feature names:", feature_names)
print("Target names:", target_names)
print("length of the iris: ", len(X))
print("\nFirst 10 rows of X:\n", X[:10])
print("\nFirst 10 rows of y:\n", y[:10])
# df_iris = pd.DataFrame(iris.data, columns=iris.feature_names)
# scatter_matrix(df_iris,figsize=(10,10))
# plt.show()

