# from PIL import Image
# from pytesseract import pytesseract
#
#
# path_to_tesseract = r"tesseract-ocr-w64-setup.exe"
# image_path = r"carPlate.jpg"
#
# img = Image.open(image_path)
#
# pytesseract.tesseract_cmd = path_to_tesseract
#
#
# text = pytesseract.image_to_string(img)
#
# print(text[:-1])

# text recognition
import cv2
import pytesseract

# read image
img = cv2.imread('carPlate.jpg')

# configurations
config = ('-l eng --oem 1 --psm 3')

# pytessercat
pytesseract.pytesseract.tesseract_cmd = 'tesseract-ocr-w64-setup.exe'
text = pytesseract.image_to_string(img, config=config)

# print text
text = text.split('\n')
print(text)
