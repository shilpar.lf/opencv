# Python program to convert from openCV2 to PIL

import cv2
from PIL import Image
opencv_image = cv2.imread("tiger-jpg.jpg",0)
color_coverted = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2RGB)
cv2.imshow("OpenCV Image", opencv_image)
pil_image = Image.fromarray(color_coverted)
pil_image.show()
cv2.waitKey(0)