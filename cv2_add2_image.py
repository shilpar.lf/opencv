
import cv2
import numpy as np

image1 = cv2.imread('img1.jpg')
image2 = cv2.imread('img2.jpg')
img2_resized = cv2.resize(image2, (image1.shape[1], image1.shape[0]))  # error fixed in this line

# weightedSum = cv2.addWeighted(image1, 0.8, img2_resized, 0.3, 0)  # add two images
sub = cv2.subtract(image1, img2_resized)    # substract two images

cv2.imshow('Weighted Image', sub)

if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
