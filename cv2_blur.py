# Python program to explain cv2.blur() method

# importing cv2
import cv2

# path
path = r'natural_image.jpg'

# Reading an image in default mode
image = cv2.imread(path)

# Window name in which image is displayed
window_name = 'Image'

ksize = (10, 5)

image = cv2.blur(image, ksize,cv2.BORDER_DEFAULT)

cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()