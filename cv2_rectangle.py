# Python program to explain cv2.line() method
import numpy as np
import cv2

path = r'images/tiger-jpg.jpg'
image = cv2.imread(path)
Img = np.zeros((512, 512, 3), dtype='uint8')

window_name = 'Image'
start_point = (0, 0)
end_point = (900, 600)
color = (170, 51, 106)
thickness = -1
image = cv2.rectangle(image, start_point, end_point, color, thickness)
cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()