import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from statsmodels.tsa.seasonal import seasonal_decompose


df = pd.read_csv('monthly_milk_production.csv',
				index_col='Date',
				parse_dates=True)
df.index.freq = 'MS'
head=df.head()
print(head)
df.plot(figsize=(12, 6))
results = seasonal_decompose(df['Production'])
plot=results.plot()
plt.show()