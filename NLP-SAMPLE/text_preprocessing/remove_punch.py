import nltk
import string
import re


def remove_punctuation(text):
    translator = str.maketrans('', '', string.punctuation)  # used to remove punctuation

    sen = text.translate(translator)
    result = re.sub(r'\d+', '', sen)  # used to remove numbers
    sentence = " ".join(result.split())  # used to remove white space

    print(sentence)


input_str = "     Hey, did you know that the summer break is coming? Amazing right !! It's only 5 more days !!"
remove_punctuation(input_str)
