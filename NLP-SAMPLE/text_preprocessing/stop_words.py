from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


# remove stopwords function
def remove_stopwords(text):
    stop_words = set(stopwords.words("english"))
    word_tokens = word_tokenize(text)
    filtered_text = [word for word in word_tokens if word not in stop_words]
    # filtered_text = []  Another way of above code
    # for word in word_tokens:
    #     if word not in stop_words:
    #         filtered_text.append(word)

    print(filtered_text)


example_text = "This is a sample sentence and we are going to remove the stopwords from this."
remove_stopwords(example_text)
