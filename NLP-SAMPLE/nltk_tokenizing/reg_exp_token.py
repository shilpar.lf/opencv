from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer("[\w']+")
text = "Let's see how it's working."
sentence=tokenizer.tokenize(text)
print(sentence)