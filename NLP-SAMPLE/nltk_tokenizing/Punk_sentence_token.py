import nltk.data

# Loading PunktSentenceTokenizer using English pickle file
tokenizer = nltk.data.load(
    'tokenizers\punkt\english.pickle')  # C:\Users\admin\AppData\Roaming\nltk_data\tokenizers\punkt
text = "Hello everyone. Welcome to GeeksforGeeks. You are studying NLP article"

sentences = tokenizer.tokenize(text)
print(sentences)
