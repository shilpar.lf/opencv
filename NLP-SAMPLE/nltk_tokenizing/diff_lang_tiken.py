import nltk.data

spanish_tokenizer = nltk.data.load('tokenizers\punkt\spanish.pickle')

text = 'Hola amigo. Estoy bien.'
sentence=spanish_tokenizer.tokenize(text)
print(sentence)