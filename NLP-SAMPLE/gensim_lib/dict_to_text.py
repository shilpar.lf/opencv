from gensim import corpora
from gensim.test.utils import get_tmpfile


with open("employee.txt", "r") as file:
    text = file.read()


tokens = text.split()
my_dictionary = corpora.Dictionary([tokens])
#
# # save your dictionary to disk
my_dictionary.save('my_dictionary.dict')
#
# # load back
load_dict1 = corpora.Dictionary.load('my_dictionary.dict')
#
print(load_dict1)


tmp_fname = get_tmpfile("dictionary")
my_dictionary.save_as_text(tmp_fname)

# load your dictionary text file
load_dict = corpora.Dictionary.load_from_text(tmp_fname)
print(load_dict)