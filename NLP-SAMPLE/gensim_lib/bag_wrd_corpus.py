from gensim import corpora



tokenized = [
    ['employee', 'work', 'employee'],
    ['manager', 'meeting', 'project'],
    ['employee', 'report', 'task'],
]
my_dictionary = corpora.Dictionary(tokenized)

# Convert tokenized documents to bag-of-words (BoW) corpus
BoW_corpus = [my_dictionary.doc2bow(doc, allow_update=True) for doc in tokenized]
print(BoW_corpus)


# [[(47, 2), (48, 1), (49, 1)], [(50, 1), (51, 1), (52, 1)], [(47, 1), (53, 1), (54, 1)]]    word_id and count

