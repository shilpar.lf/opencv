import gensim.downloader as api

# check available models and datasets
info_datasets = api.info()
print(info_datasets)

# information of a particular dataset
dataset_info = api.info("text8")

# load the "text8" dataset
dataset = api.load("text8")

# load a pre-trained model
word2vec_model = api.load('word2vec-google-news-300')
