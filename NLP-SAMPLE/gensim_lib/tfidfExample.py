
from gensim import models
from gensim import corpora
import numpy as np

with open("employee.txt", "r") as file:
    text = file.read()

tokens = text.split()
my_dictionary = corpora.Dictionary([tokens])
my_dictionary.save('my_dictionary.dict')

loaded_dict = corpora.Dictionary.load('my_dictionary.dict')

# Assuming you have a list of tokenized documents (list of lists)
# Replace the following 'tokenized' placeholder example with your actual tokenized documents
tokenized = [
    ['employee', 'work', 'office'],
    ['manager', 'meeting', 'project'],
    ['employee', 'report', 'task'],
]

# Convert tokenized documents to bag-of-words (BoW) corpus using the dictionary
BoW_corpus = [my_dictionary.doc2bow(doc, allow_update=True) for doc in tokenized]

# Create TF-IDF model
tfIdf = models.TfidfModel(BoW_corpus, smartirs='ntc')

# Calculate TF-IDF word weights
weight_tfidf = []
for doc in tfIdf[BoW_corpus]:
    for id, freq in doc:
        weight_tfidf.append([my_dictionary[id], np.around(freq, decimals=3)])

print(weight_tfidf)
