import gensim
import os
from gensim.utils import simple_preprocess

# open the text file as an object
doc = open('employee.txt', encoding='utf-8')
# text = "café résumé crème brûlée"
# sam = simple_preprocess(text)
# print(sam)
# preprocess the file to get a list of tokens
tokenized = []
for sentence in doc.read().split('.'):
    tokenized.append(simple_preprocess(sentence, deacc=True))  # de-accents return data easy to read // simple_preprocess is to convert a text document into a list of tokens.

print(tokenized)
