import gensim
import os
from gensim.utils import simple_preprocess
from gensim import corpora

doc = open('employee.txt', encoding='utf-8')

# preprocess the file to get a list of tokens
tokenized = []
for sentence in doc.read().split('.'):
    tokenized.append(simple_preprocess(sentence, deacc=True))

print(tokenized)
my_dictionary = corpora.Dictionary(tokenized)
print(my_dictionary)
