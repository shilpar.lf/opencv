from gensim import corpora

from gensim.corpora import MmCorpus
from gensim.test.utils import get_tmpfile

# Assuming you have defined BoW_corpus properly
BoW_corpus = [
    [(47, 1), (48, 1), (0, 0)],
    [(50, 1), (51, 1), (52, 1)],
    [(47, 1), (53, 1), (54, 1)]
]

output_fname = get_tmpfile("BoW_corpus.mm") #The get_tmpfile function is used to create a temporary file path with the specified prefix ("BoW_corpus.mm" in this case). The temporary file will be used to save the serialized BoW corpus.

# save corpus to disk
MmCorpus.serialize(output_fname, BoW_corpus) # the file path where the corpus will be saved (output_fname),

# load corpus
load_corpus = MmCorpus(output_fname) #used to load the serialized BoW corpus from disk and store it in the load_corpus // MmCorpus is a class in the Gensim library that represents a sparse matrix-based corpus in the Matrix Market (MM) format

print(load_corpus)
