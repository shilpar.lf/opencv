import gensim.downloader as api
from multiprocessing import cpu_count
from gensim.models.word2vec import Word2Vec

# Load the text8 dataset
dataset = api.load(
    "text8")  # "text8" dataset is a small, pre-processed version of the English Wikipedia dump containing about 17
# million words.
# 17 million words are in text8
# Extract a list of words from the dataset
data = []
for word in dataset:
    data.append(word)

# We will split the data into two parts
data_1 = data[:1200]  # This is used to train the model
data_2 = data[1200:]  # This part will be used to update the model

# Training the Word2Vec model
w2v_model = Word2Vec(data_1, min_count=0, workers=cpu_count())

# Word vector for the word "time" after training the model
word_vector = w2v_model.wv['time']
print(word_vector)

# similar words to the word "time"
print(w2v_model.most_similar('time'))

# save your model
w2v_model.save('Word2VecModel')

# load your model
model = Word2Vec.load('Word2VecModel')
