import spacy

# loading modules to the pipeline.
nlp = spacy.load("en_core_web_sm")

# Initialising doc with a sentence.
doc = nlp("If you want to be an excellent programmer \
, be consistent to practice daily on GFG.")

# Using properties of token i.e. Part of Speech and Lemmatization
for token in doc:
    print(token, " | ",
          spacy.explain(token.pos_),   # if only give token.pos_ than it returns only short form of token
          " | ", token.lemma_)
