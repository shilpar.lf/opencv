from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from functools import reduce

ps = PorterStemmer()

sentence = "leaves"
words = word_tokenize(sentence)

# using reduce to apply stemmer to each word and join them back into a string
stemmed_sentence = reduce(lambda x, y: x + " " + ps.stem(y), words, "")

print(stemmed_sentence)
#This code is contrinuted by Pushpa.



# Initial value of x: ""
# First iteration: x = "" + " " + ps.stem("running") = " run"
# Second iteration: x = " run" + " " + ps.stem("jumps") = " run jump"
# Third iteration: x = " run jump" + " " + ps.stem("playing") = " run jump play"
# Fourth iteration: x = " run jump play" + " " + ps.stem("jumped") = " run jump play jump"