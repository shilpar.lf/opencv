from gtts import gTTS
import os
import pygame


def play_audio(filename):
    pygame.mixer.init()
    pygame.mixer.music.load(filename)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        pygame.time.Clock().tick(10)


def main():
    # Get input from the user
    mytext = input("Enter the text you want to convert to speech: ")

    # Language in which you want to convert
    language = 'en'

    # Passing the text and language to the engine
    myobj = gTTS(text=mytext, lang=language, slow=False)

    # Save the converted audio in a mp3 file named output.mp3
    myobj.save("output.mp3")

    # Play the converted audio using pygame
    play_audio("output.mp3")

    # Close the pygame mixer after audio playback
    pygame.mixer.quit()

    # Now remove the temporary audio file
    try:
        os.remove("output.mp3")
    except Exception as e:
        print(f"Error while deleting file: {e}")


if __name__ == "__main__":
    main()
