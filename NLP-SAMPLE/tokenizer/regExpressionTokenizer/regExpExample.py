# import RegexpTokenizer() method from nltk
from nltk.tokenize import RegexpTokenizer

# Create a reference variable for Class RegexpTokenizer
tk = RegexpTokenizer('\s+', gaps=True)

# Create a string input
gfg = " we are able to extract the tokens from string by using regular expression with"

# Use tokenize method
geek = tk.tokenize(gfg)

print(geek)
