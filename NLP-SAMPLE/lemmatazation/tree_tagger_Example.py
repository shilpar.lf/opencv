import pandas as pd
import treetaggerwrapper as tt

t_tagger = tt.TreeTagger(TAGLANG='en', TAGDIR=r'C:\TreeTagger')  # TAGLANG is a tag language "TAGDIR" represent where
# the tree tagger is installed in our system.

pos_tags = t_tagger.tag_text("the bats saw the cats with best stripes hanging upside down by their feet")
# "tag_text is method to perform POS[part of speech] . It takes a string (text or sentence) as input and returns a
# list of tuples, where each tuple contains the word, its associated lemma (base form), and its corresponding
# part-of-speech (POS) tag.
original = []
lemmas = []
tags = []
for t in pos_tags:
    original.append(t.split('\t')[0])
    tags.append(t.split('\t')[1])
    lemmas.append(t.split('\t')[2])

Results = pd.DataFrame({'Original': original, 'Lemma': lemmas, 'Tags': tags})
print(Results)
