from stanfordcorenlp import StanfordCoreNLP
import json

nlp = StanfordCoreNLP('http://localhost', port=9000, timeout=30)

props = {'annotators': 'pos, lemma', 'pipelineLanguage': 'en', 'outputFormat': 'json'}

sentence = "the bats saw the cats with best stripes hanging upside down by their feet"
parsed_str = nlp.annotate(sentence, properties=props) #input,properties:optional (it uses language of text ot output format)
print(parsed_str)