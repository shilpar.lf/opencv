import pattern
from pattern.en import lemma, lexeme
from pattern.en import parse

sentence = "the bats saw the cats with best stripes hanging upside down by their feet"

lemmatized_sentence = " ".join([lemma(word) for word in sentence.split()])

print(lemmatized_sentence)

all_lemmas_for_each_word = [lexeme(wd) for wd in sentence.split()]
print(all_lemmas_for_each_word)

