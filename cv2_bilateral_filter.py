import cv2

img = cv2.imread('taj.jpg')

# bilateral = cv2.bilateralFilter(img, 115, 175, 175)
# bilateral=cv2.blur(img, (5, 5))
bilateral=cv2.GaussianBlur(img, (5, 5), 0)
gray = cv2.cvtColor(bilateral, cv2.COLOR_BGR2GRAY)
sobelx = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)
# bilateral=cv2.medianBlur(img, 5)

# Save the output.
taj=cv2.imwrite( "taj.jpg",sobelx)
cv2.imshow('taj',img)
cv2.waitKey(0)
cv2.destroyAllWindows()