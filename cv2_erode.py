
# importing cv2
import cv2

# importing numpy
import numpy as np

path = r'images/tiger-jpg.jpg'

image = cv2.imread(path)

window_name = 'Image'

kernel = np.ones((10, 20), np.uint8)

image = cv2.erode(image, kernel, cv2.BORDER_REFLECT)

cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()