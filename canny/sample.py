import cv2

pic=r'C:\Users\admin\PycharmProjects\pythonProject\images\carPlate2.jpg'
img = cv2.imread(pic) # Read image

# Setting parameter values
t_lower = 50# Lower Threshold
t_upper = 150 # Upper threshold

# Applying the Canny Edge filter
edge = cv2.Canny(img, t_lower, t_upper)

cv2.imshow('original', img)
cv2.imshow('edge', edge)
cv2.waitKey(0)
cv2.destroyAllWindows()
