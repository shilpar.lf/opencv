import os
import numpy as np
import cv2
import imutils
import sys
import pytesseract
import logging

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

log_filename = os.path.join(os.getcwd(), 'npr.log')
logging.basicConfig(filename=log_filename, filemode='a', level=logging.DEBUG,
                    format='%(asctime)s-%(levelname)s:%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')
logger = logging.getLogger("npr")


def image_process(input_image, width=500, psm=3, show_images=False):
    logger.info("Image: %s, width: %s, psm: %s", input_image, width, psm)
    image = cv2.imread(input_image)

    image = imutils.resize(image, width)  # 600, 500

    cv2.imshow("Original Image", image)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("1 - Grayscale Conversion", gray)

    gray = cv2.bilateralFilter(gray, 11, 17, 17)  #(src,distance, sigmaColor,sigmaSpace) usage(Reduction of noise + Preserving of edges),in noise picture medialfilter is best and other picture gaussian is best
    cv2.imshow("2 - Bilateral Filter", gray)

    edged = cv2.Canny(gray, 170, 200)     #(src,threshold1[minimum gradient intencity],threshold2[maximum gradient intencity])
    cv2.imshow("4 - Canny Edges", edged)

    counts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0] # zero represent first element in the contours list

    counts = sorted(counts, key=cv2.contourArea, reverse=True)[:30]

    number_plate_cnt = None

    for c in counts:
        peri = cv2.arcLength(c, True)    # used to calculate the perimeter[add up all four side lengths] of the contour.
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)  # find approximation shape of contours.(curve,epsilon,closed)
        print(approx)
        if len(approx) == 4:
            number_plate_cnt = approx
            break

    if number_plate_cnt is not None:
        # Masking the part other than the number plate
        mask = np.zeros(gray.shape, np.uint8)  # dimention of image, unsigned 8 bit integer
        # new_image = cv2.drawContours(mask, [number_plate_cnt], 0, 255, -1)
        cv2.drawContours(mask, [number_plate_cnt], 0, 255, -1)    # (image, contours, contoursIndex [0] (specify the first contours), color, thickness)
        # cv2.imshow("image",aa)
        # xx = cv2.boundingRect(number_plate_cnt)
        # cv2.imshow("show",xx)

        x, y, w, h = cv2.boundingRect(number_plate_cnt)
        # Crop the number plate region from the original image
        cropped_number_plate = image[y:y + h, x:x + w]
        # new_image = cv2.bitwise_and(image, image, mask=mask)

        if show_images:
            cv2.imshow("Number Plate Image", cropped_number_plate)
            cv2.waitKey(0)  # Wait for a key press
            cv2.destroyAllWindows()
        # Configuration for tesseract
        config = ('-l eng --oem 1 --psm {psm}'.format(psm=psm))  #(-l eng = english language ,-oem = OCR Engine Mode , --psm = page segmentation mode)\\ behaviour of the tessarect

        # Run tesseract OCR on image
        text = pytesseract.image_to_string(cropped_number_plate, config=config)

        # return recognized text
        return text

    return ""


if __name__ == "__main__":

    input_img = r'C:\Users\admin\PycharmProjects\pythonProject\images\carPlate2.jpg'

    npr = image_process(input_img, show_images=True)
    if npr == "":
        npr = image_process(input_img, width=800, show_images=True)
    if npr == "":
        npr = image_process(input_img, psm=10, show_images=True)
    print(npr)
