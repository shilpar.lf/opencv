
import cv2 as cv
import numpy as np
img1 = cv.imread('natural_image.jpg')
img2 = cv.imread('tiger_jpg.jpg')
# img1_resized = cv2.resize(img1, (img2.shape[1], img2.shape[0]))
dest_and = cv.bitwise_and(img1, img2, mask=None)

cv.imshow('Bitwise And', dest_and)

if cv.waitKey(0) & 0xff == 27:
    cv.destroyAllWindows()
