import cv2
import numpy as np
from numpy import savetxt
# Load YOLO
# net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
net = cv2.dnn.readNet(r"C:\Users\admin\Downloads\yolov3.weights",
                     r"C:\Users\admin\PycharmProjects\pythonProject\yolov3.cfg")
classes = []
with open(r"C:\Users\admin\PycharmProjects\pythonProject\coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames()
output_layers = [layer_names[i - 1] for i in net.getUnconnectedOutLayers()]

# Load video
video_path = r"C:\Users\admin\PycharmProjects\pythonProject\images\car_video2.mp4"
cap = cv2.VideoCapture(video_path)

# Define output video codec and create VideoWriter object
output_path = "output_video.avi"
fourcc = cv2.VideoWriter_fourcc(*'XVID')   #**************************
fps = cap.get(cv2.CAP_PROP_FPS)
# print(fps)
frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
outer = cv2.VideoWriter(output_path, fourcc, fps, (frame_width, frame_height))

# Process video frames
while True:
    ret, frame = cap.read()

    if not ret:
        break

    height, width, channels = frame.shape

    # Perform object detection
    blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(output_layers)

    # Process the results
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:  # Adjust the confidence threshold as needed
                # Calculate the bounding box coordinates
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Calculate the top-left corner coordinates of the bounding box
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    # Apply non-maximum suppression to remove redundant overlapping boxes
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)  # Adjust the parameters if needed

    font = cv2.FONT_HERSHEY_SIMPLEX
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            confidence = confidences[i]
            color = (0, 255, 0)  # Green color for bounding box

            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y - 10), font, 0.5, color, 2)

    # Write the frame with bounding boxes to the output video
    # out.append(frame)
    outer.write(frame)
    # np.where(out == frame)

    # Display the resulting frame
    cv2.imshow("Object Detection", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the resources
cap.release()
outer.release()
cv2.destroyAllWindows()

