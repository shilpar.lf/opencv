# import urllib3
# resp = urllib3.request("GET", "https://httpbin.org/robots.txt")
# resp.status

import urllib3
#
# print(urllib3.__version__)

# import urllib3


http = urllib3.PoolManager()

url = 'http://webcode.me'

# resp = http.request('GET', url)
# print(resp.status)

resp = http.request('HEAD', url)

print(resp.headers['Server'])
print(resp.headers['Date'])
print(resp.headers['Content-Type'])
print(resp.headers['Last-Modified'])