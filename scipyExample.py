# import numpy as np
# from scipy.sparse import csr_matrix
#
# arr = np.array([0, 0, 0, 0, 0, 1, 1, 0, 2])
#
# print(csr_matrix(arr))
# import numpy as np
# from scipy.sparse import csr_matrix
#
# arr = np.array([[0, 0, 0], [0, 0, 1], [1, 0, 2]])
#
# print(csr_matrix(arr).data)

import sys
import matplotlib
matplotlib.use('Agg')

import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt

matplotlib.use( 'tkagg' )     #important line

points = np.array([
  [2, 4],
  [3, 4],
  [3, 0],
  [2, 2],
  [4, 1]
])

simplices = Delaunay(points).simplices

plt.triplot(points[:, 0], points[:, 1], simplices)
plt.scatter(points[:, 0], points[:, 1], color='r')

plt.show()

plt.savefig(sys.stdout.buffer)
sys.stdout.flush()