import cv2
import numpy as np
from PIL import Image
import pytesseract
from pytesseract import Output
import os

# import easyocr
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
# import cv2


# Function to extract frames
# program to capture single image from webcam in python

# importing OpenCV library


# Read input image
# image = cv2.imread("carPlate2.jpg")
image = cv2.imread(r'C:\Users\admin\PycharmProjects\pythonProject\images\carPlate2.jpg', cv2.IMREAD_UNCHANGED)


# convert input image to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# read haarcascade for number plate detection
cascade = cv2.CascadeClassifier('haarcascades_haarcascade_russian_plate_number.xml')

# Detect license number plates
plates = cascade.detectMultiScale(image, 3.3, 7)
print('Number of detected license plates:', len(plates))
# print("number plate numbers: ", result)

# loop over all plates
for (x, y, w, h) in plates:
    # draw bounding rectangle around the license number plate
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    gray_plates = image[y:y + h, x:x + w]
    color_plates = image[y:y + h, x:x + w]

    # save number plate detected
    cv2.imwrite('Numberplate1.jpg', gray_plates)
    cv2.imshow('Number Plate1', gray_plates)
    cv2.imshow('Number Plate Image1', gray)
text = pytesseract.image_to_string(gray_plates)

print("plate number: ", text[:-1])

cv2.waitKey(0)
cv2.destroyAllWindows()

# import cv2
# import numpy as np
# import pytesseract
# from pytesseract import Output
#
#
# image = cv2.imread('carPlate2.jpg')
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# cascade = cv2.CascadeClassifier('haarcascades_haarcascade_russian_plate_number.xml')
# plates = cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
# text = pytesseract.image_to_string(image)
# for (x, y, w, h) in plates:
#     cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
# print('Number of detected license plates:', len(plates))
# print("plate number: ", text)
#
# cv2.imshow("number plate detection ", image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
