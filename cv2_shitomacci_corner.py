# Python program to illustrate
# corner detection with
# Shi-Tomasi Detection Method

# organizing imports
import cv2
import inline as inline
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline    #[this line put the error so command this line and add plt.show() in the last]


img = cv2.imread('natural_image.jpg')

gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


corners = cv2.goodFeaturesToTrack(gray_img, 100, 0.05, 10) # first=number of dot second= third=gap between dots

corners = np.int0(corners)

for i in corners:
    x, y = i.ravel()
    cv2.circle(img, (x, y), 3, (255, 0, 0), -1)

plt.imshow(img)

if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
plt.show()
