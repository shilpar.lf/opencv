# import the asyncio library
import asyncio

# define a TimeCount function using async
async def TimeCount():
    print("It is 1'oclock")
    await asyncio.sleep(1)
    print("It is 2'oclock")
    await asyncio.sleep(1)
    print("It is 3'oclock")
    await asyncio.sleep(1)
    print("It is 4'oclock")
    await asyncio.sleep(1)
    print("It is 5'oclock")
# lets create a main function
async def main():
    # we will pass the TimeCount() into the asyncio gather functio
    await asyncio.gather(TimeCount())

if __name__ == "__main__":
    import time
    tpc = time.perf_counter()
    asyncio.run(main())
    Timeup = time.perf_counter() - tpc
    print(f"{__file__} executed in {Timeup:0.2f} seconds.")