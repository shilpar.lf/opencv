# Python program to explain cv2.copyMakeBorder() method

import cv2
path = r'images/tiger-jpg.jpg'
image = cv2.imread(path)
window_name = 'Image'
top=100
bottom=100
left=50
right=50
borderType=cv2.BORDER_REPLICATE
image = cv2.copyMakeBorder(image, top,bottom , left, right, borderType,value=0)

cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()