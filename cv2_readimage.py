# Python program to read image as RGB

# Importing cv2 and matplotlib module
import cv2
import matplotlib.pyplot as plt

path = r'text.png'
img = cv2.imread(path)

plt.imshow(img)
cv2.waitKey(0)
cv2.destroyAllWindows()


# Python program to visualize
# Heat map of image

# import matplotlib.pyplot as plt
# import cv2
#
# img = cv2.imread('text.png')
#
# plt.imshow(img, cmap ='hot')
# cv2.waitKey(0)
# cv2.destroyAllWindows()




