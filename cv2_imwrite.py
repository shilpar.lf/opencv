import cv2
import os
image_path = r'C:\Users\admin\Desktop\tiger-jpg.jpg'
directory = r'C:\Users\admin\Desktop'
img = cv2.imread(image_path)
os.chdir(directory)
print("Before saving image:")
print(os.listdir(directory))
filename = 'savedImage.png'
cv2.imwrite(filename, img)
print("After saving image:")
print(os.listdir(directory))
print('Successfully saved')
cv2.destroyAllWindows()
