# import cv2
#
# # Read the input image
# img = cv2.imread('c_area.png')
#
# # convert the image to grayscale
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#
# # Apply thresholding in the gray image to create a binary image
# ret,thresh = cv2.threshold(gray,150,255,0)
#
# # Find the contours using binary image
# contours,hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
# print("Number of contours in image:",len(contours))
# cnt = contours[0]
#
# # compute the area and perimeter
# area = cv2.contourArea(cnt)
# perimeter = cv2.arcLength(cnt, True)
# perimeter = round(perimeter, 4)
# print('Area:', area)
# print('Perimeter:', perimeter)
# img1 = cv2.drawContours(img, [cnt], -1, (0,255,255), 3)
# x1, y1 = cnt[0,0]
# cv2.putText(img1, f'Area:{area}', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
# cv2.putText(img1, f'Perimeter:{perimeter}', (x1, y1+20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
#
# cv2.imshow("Image", img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

import cv2
import numpy as np

img1 = cv2.imread('c_area.png')
img = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img, 10, 255, 0)
contours, hierarchy = cv2.findContours(thresh, 1, 2)
print("Number of contours in image:", len(contours))

for i, cnt in enumerate(contours):
    M = cv2.moments(cnt)
    if M['m00'] != 0.0:
        x1 = int(M['m10'] / M['m00'])
        y1 = int(M['m01'] / M['m00'])
    area = cv2.contourArea(cnt)
    perimeter = cv2.arcLength(cnt, True)
    perimeter = round(perimeter, 4)
    print(f'Area of contour {i + 1}:', area)
    print(f'Perimeter of contour {i + 1}:', perimeter)
    img1 = cv2.drawContours(img1, [cnt], -1, (255, 255, 255), 5)   # draw the rectangle around the image
    cv2.putText(img1, f'Area :{area}', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
    cv2.putText(img1, f'Perimeter :{perimeter}', (x1, y1 + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 255), 2)

cv2.imshow("Image", img1)
cv2.waitKey(0)
cv2.destroyAllWindows()
