import threading
import time


def display(x, s, name):
    for i in range(x):
        time.sleep(s)
        print(name, "::Thread Started...")


t = threading.Thread(target=display, args=(5, 2, "THREAD1",))  # 15 is x/range value , 2 is t1/sleep value
t.start()  # start the thread
t1 = threading.Thread(target=display, args=(5, 2, "THREAD2",))
t1.start()
