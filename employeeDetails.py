class Employee:
    def __init__(self, id, name, gender, post):
        self.id = id
        self.name = name
        self.gender = gender
        self.post = post


class EmployeeDatabase:
    def __init__(self):
        self.employees = []

    # def employee_list(self,employee):
    #     self.employees.append(employee)

    def add_employees(self, employees):
        self.employees.extend(employees)

    def add_employee(self, id, name, gender, post):
        # self.employees.append(employees)
        employee = Employee(id, name, gender, post)
        self.employees.append(employee)
        print("Employee added successfully...")
        # print(x)

    def delete_employee(self, id):
        for employee in self.employees:
            if employee.id.lower() == id.lower():
                self.employees.remove(employee)
                print("Employee deleted successfully...")
                return
        print("Employee not found.")

    def search_employee(self, field, keyword):
        matching_employees = []

        if field == "name":
            # matching_employees = [employee for employee in self.employees if keyword.lower() in employee.name.lower()]
            matching_employees = [employee for employee in self.employees if
                                  employee.name.lower().startswith(keyword.lower())]


        elif field == "id":
            matching_employees = [employee for employee in self.employees if str(employee.id) == keyword]

        if field == "gender":
            male_employees = [employee for employee in self.employees if employee.gender.lower() == "male"]
            female_employees = [employee for employee in self.employees if employee.gender.lower() == "female"]

            if keyword.lower() == 'male' or keyword.lower() == 'm':
                matching_employees = male_employees
            elif keyword.lower() == 'female' or keyword.lower() == 'f':
                matching_employees = female_employees

        elif field == "post":
            # matching_employees = [employee for employee in self.employees if keyword.lower() in employee.post.lower()]
            matching_employees = [employee for employee in self.employees if
                                  employee.post.lower().startswith(keyword.lower())]

        return matching_employees

    def employee_details(self):
        if len(self.employees) > 0:
            print("Employee Details........")
            for employee in self.employees:
                print(f"Employee ID: {employee.id}")
                print(f"Name: {employee.name}")
                print(f"Gender: {employee.gender}")
                print(f"Post: {employee.post}")
                print()
        else:
            print("No employees found.")


database = EmployeeDatabase()

employee_list = [
    Employee("101", "Raj", "Male", "Manager"),
    Employee("102", "Janani", "Female", "Python Developer"),
    Employee("103", "Ram", "Male", "Web Design"),
    Employee("104", "Sheela", "Female", "Manager")

]
database.add_employees(employee_list)

while True:
    print("\n*****MENU*****:")
    print("1. Add Employee")
    print("2. Delete Employee")
    print("3. Search Employee ")
    # print("4. Search by gender")
    print("4. Employee Details ")

    print("5. Exit")
    choice = int(input("Enter your choice (1-4): "))

    if choice == 1:
        id = input("Enter employee id: ")
        name = input("Enter employee name: ")
        gender = input("Enter employee gender: ")
        post = input("Enter employee post: ")
        database.add_employee(id, name, gender, post)

    elif choice == 2:
        name = input("Enter employee id: ")
        database.delete_employee(id)

    # elif choice == 4:
    #     gender = input("Enter gender to search: ")
    #     database.search_employee_by_gender(gender)

    elif choice == 3:
        field = input("Choose The Field To Search (id,name,gender,post) : ")

        keyword = input("Enter " + field + " :")
        result = database.search_employee(field, keyword)
        # if keyword== field:
        if len(result) > 0:
            print("Matching employees found:")
            for employee in result:
                print(
                    f"Employee ID: {employee.id}, Name: {employee.name},  Gender:{employee.gender},Position: {employee.post}")
        else:
            print("No matching employees found.")

    elif choice == 4:
        database.employee_details()


    elif choice == 5:
        break

    else:
        print("Invalid choice. Please try again.")
