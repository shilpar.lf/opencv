import cv2
import numpy as np
import pytesseract

# Configure pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Load YOLO network
net = cv2.dnn.readNetFromDarknet(r"C:\Users\admin\PycharmProjects\pythonProject\yolov3.cfg",
                                 r"C:\Users\admin\Downloads\yolov3.weights")

# Load video
video = cv2.VideoCapture(r"C:\Users\admin\PycharmProjects\pythonProject\images\car_video4.mp4")
frameTime = 1
# Define classes
classes = ["number plate"]

while True:
    ret, frame = video.read()
    if not ret:
        break

    height, width, _ = frame.shape

    # Create a blob from the frame and set it as input to the network
    blob = cv2.dnn.blobFromImage(frame, 1 / 255, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)

    # Perform forward pass through the network
    output_layers_names = net.getUnconnectedOutLayersNames()
    layer_outputs = net.forward(output_layers_names)

    # Extract bounding box information and filter out number plates
    boxes = []
    confidences = []
    class_ids = []
    for output in layer_outputs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > 0.5 and class_id == 0:  # Class index for number plate
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    # Apply non-maximum suppression to remove overlapping bounding boxes
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    # **********************************************************************888
    # # Iterate over the detected number plates
    # for i in indexes:
    #     # index = index[0]
    #     # i = i[0]
    #     i = boxes[0]
    #     # boxes = frame[i]
    #     x, y, w, h = i[0], i[1], i[2], i[3]
    #     # Crop the number plate region from the frame
    #     plate_image = frame[y:y + h, x:x + w]
    #
    #     # Convert the number plate region to grayscale
    #     gray_plate = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
    #
    #     # Apply image preprocessing if required
    #     # ...
    #
    #     # Perform OCR on the number plate image
    #     plate_text = pytesseract.image_to_string(gray_plate, config='--psm 7')
    #
    #     # Draw bounding box around the number plate
    #     cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    #
    #     # Print the detected number plate number
    #     print("Detected Number Plate:", plate_text)
    # **************************************************************

    for (x, y, w, h) in indexes:
        # draw bounding rectangle around the license number plate
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # gray_plates = frame[y:y + h, x:x + w]
        plate_image = frame[y:y + h, x:x + w]
        gray_plate = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)

        color_plates = frame[y:y + h, x:x + w]

        # save number plate detected
        cv2.imwrite('Numberplate1.jpg', gray_plates)
        cv2.imshow('Number Plate1', gray_plates)
        cv2.imshow('Number Plate Image1', gray)
        # text = pytesseract.image_to_string(gray_plates)
        plate_text = pytesseract.image_to_string(gray_plate, config='--psm 7')

        print("plate number: ", text[:-1])
    # Display the resulting frame
    cv2.imshow("Number Plate Detection", frame)

    # Exit if 'q' is pressed
    if cv2.waitKey(frameTime) & 0xFF == ord('q'):
        break

# Release video capture and close windows
video.release()
cv2.destroyAllWindows()
