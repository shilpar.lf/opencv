import re

txt = "The023 rain24 in25 Spain26"

# Return a match at every word character (characters from a to Z, digits from 0-9, and the underscore _ character):
# reg_exp1 = '\w{2}\d{2}\w{2}\d{4}'

x = re.findall("\w{2}.\d{2}.\w{2}", txt)

print(x)
