

from scipy.misc import imread, imsave, imresize

img = imread('cat.jpg') # path of the image
print(img.dtype, img.shape)

img_tint = img * [1, 0.45, 0.3]

imsave('cat_tinted.jpg', img_tint)

img_tint_resize = imresize(img_tint, (300, 300))

imsave('cat_tinted_resized.jpg', img_tint_resize)
