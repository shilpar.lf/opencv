# Python program to explain cv2.putText() method

import cv2

path = r'images/tiger-jpg.jpg'
image = cv2.imread(path)
window_name = 'Image'
text="ONE MAN ARMY"
font = cv2.FONT_HERSHEY_TRIPLEX = 4  # font style
org = (300, 60)  # inside and upper length
fontScale = 1  # font size
color = (255, 0, 0)  # color of the font
thickness = 2
image = cv2.putText(image, text, org, font,
                    fontScale, color, thickness,cv2.LINE_AA,False) #False or True
cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()
