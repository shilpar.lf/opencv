# Python program to explain cv2.imshow() method

#importing cv2

import cv2

path = r'C:\Users\admin\Desktop\natural_image.jpg'
image = cv2.imread(path)
window_name = 'image'
cv2.imshow(window_name, image)
cv2.waitKey(0)
cv2.destroyAllWindows()